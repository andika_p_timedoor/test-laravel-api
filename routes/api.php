<?php

use Illuminate\Http\Request;
use App\UserProfile;
use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/user/1', function (User $user) {
	$user->name  = 'Andika';
	$user->email = 'andika_p@timedoor.net';

	$profile = new UserProfile();
	$profile->gender = 'man';
	$profile->phone  = '812336622';

	$user->profile()->associate($profile);

    return [
    	"data" => $user->jsonSerialize()
    ];
});

Route::get('/user', function () {
	$userA = new User();
	$userB = new User();
	$userC = new User();
	$userA->name  = 'Andika';
	$userA->email = 'andika_p@timedoor.net';

	$profileA = new UserProfile();
	$profileA->gender = 'apache-helikopter';
	$profileA->phone  = '811111111';
	
	$userA->profile()->associate($profileA);

	$userB->name  = 'Cindy';
	$userB->email = 'cindy@timedoor.net';

	$profileB = new UserProfile();
	$profileB->gender = 'woman';
	$profileB->phone  = '8122222222';
	
	$userB->profile()->associate($profileB);

	$userC->name  = 'Kevin';
	$userC->email = 'kevin@timedoor.net';

	$profileC = new UserProfile();
	$profileC->gender = 'man';
	$profileC->phone  = '81233333333';
	
	$userC->profile()->associate($profileC);

	$collection = collect([$userA, $userB, $userC]);

    return [
    	"data" => $collection->jsonSerialize()
    ];
});
